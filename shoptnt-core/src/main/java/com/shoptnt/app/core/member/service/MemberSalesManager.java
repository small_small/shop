/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.member.service;

import com.shoptnt.app.core.member.model.vo.SalesVO;
import com.shoptnt.app.framework.database.Page;

/**
 * 会员销售记录
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018/6/29 上午9:31
 * @Description:
 *
 */
public interface MemberSalesManager {


    /**
     * 商品销售记录
     * @param pageSize
     * @param pageNo
     * @param goodsId
     * @return
     */
    Page<SalesVO> list(Integer pageSize, Integer pageNo, Integer goodsId);


}
