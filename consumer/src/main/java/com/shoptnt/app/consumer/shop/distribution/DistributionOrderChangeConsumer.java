/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.shop.distribution;

import com.shoptnt.app.consumer.core.event.OrderStatusChangeEvent;
import com.shoptnt.app.core.base.message.OrderStatusChangeMsg;
import com.shoptnt.app.core.client.distribution.DistributionOrderClient;
import com.shoptnt.app.core.trade.order.model.dos.OrderDO;
import com.shoptnt.app.core.trade.order.model.enums.OrderStatusEnum;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 分销商订单处理
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/14 上午7:13
 */

@Component
public class DistributionOrderChangeConsumer implements OrderStatusChangeEvent {

    @Autowired
    private DistributionOrderClient distributionOrderClient;

    protected final Log logger = LogFactory.getLog(this.getClass());


    @Override
    public void orderChange(OrderStatusChangeMsg orderStatusChangeMsg) {
        OrderDO order = orderStatusChangeMsg.getOrderDO();
        try {
            if (orderStatusChangeMsg.getNewStatus().equals(OrderStatusEnum.ROG)) {
                distributionOrderClient.confirm(order);
            }
        } catch (Exception e) {
            logger.error("订单收款分销计算返利异常：", e);
        }
    }

}
