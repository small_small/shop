/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.statistics.impl;

import com.shoptnt.app.core.client.statistics.SyncopateTableClient;
import com.shoptnt.app.core.statistics.service.SyncopateTableManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 统计数据填充
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-22 上午8:41
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class SyncopateTableDefaultImpl implements SyncopateTableClient {
    @Autowired
    private SyncopateTableManager syncopateTableManager;

    /**
     * 每日填充数据
     */
    @Override
    public void everyDay() {
        syncopateTableManager.everyDay();
    }

}
