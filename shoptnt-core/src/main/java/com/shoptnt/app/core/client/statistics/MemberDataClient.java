/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.statistics;

import com.shoptnt.app.core.member.model.dos.Member;

/**
 * 会员信息收集manager
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午4:10
 */

public interface MemberDataClient {

    /**
     * 会员注册
     *
     * @param member 会员
     */
    void register(Member member);


}
